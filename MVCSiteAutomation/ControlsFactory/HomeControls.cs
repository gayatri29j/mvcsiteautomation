﻿using System.Collections.Generic;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace MVCSiteAutomation.ControlsFactory
{
    class HomeControls
    {
        public HomeControls()
        {
            //Initializing the PageFactory
            PageFactory.InitElements(PropertyCollection.driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//ul[@class='js-paddle-items']/child::li")]
        public IList<IWebElement> linksMenu { get; set; }

        [FindsBy(How = How.Id, Using = "shellmenu_0")]
        public IWebElement linkOffice { get; set; }

        [FindsBy(How = How.LinkText, Using = "Windows")]
        public IWebElement linkWindows { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[text()='Surface']")]
        public IWebElement linkSurface { get; set; }

        [FindsBy(How = How.XPath, Using = "//ul[@class='js-paddle-items']/child::li[4]/a")]
        protected IWebElement Xbox { get; set; }

    }
}
