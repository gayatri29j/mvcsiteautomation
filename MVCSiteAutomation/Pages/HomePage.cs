﻿using MVCSiteAutomation.ControlsFactory;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace MVCSiteAutomation.Pages
{
    class HomePage : PageBase
    {
        //Initialize the Page by creating the object
        HomeControls homeControls = new HomeControls();

        public int MainMenuItemsCount()
        {
            IList<IWebElement> MenuList = homeControls.linksMenu;
            return MenuList.Count;
        }

    }
}
