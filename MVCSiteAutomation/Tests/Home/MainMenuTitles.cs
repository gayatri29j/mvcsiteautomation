﻿using MVCAutomation.Utils;
using MVCSiteAutomation.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Test Case Steps  
 * 1. Login into https://www.microsoft.com/en-in/
 * 2. Get number of main menu titles 
 * 3. Verify the menu titles 
 */

namespace MVCSiteAutomation.Tests.Home
{
    [TestFixture]
    public class MainMenuTitles : TestBase
    {
        [Test]
        public void GetMenuLinksCount()
        {
            HomePage homePage = new HomePage();

            int menuCount = homePage.MainMenuItemsCount();
            Console.WriteLine("Number of menu titles are " + menuCount);

            // TODO: Add your test code here
            Assert.Pass("Your first passing test");
        }
    }
}
