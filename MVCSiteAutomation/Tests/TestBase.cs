﻿using MVCAutomation.Utils;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCSiteAutomation.Tests
{
    [TestFixture]
    public class TestBase
    {
        [SetUp]
        protected void InitializeBrowser()
        {
            Console.WriteLine("Initialize Browser called");
            //Initializing the driver based on the browser type.
            SingletonDriver.InitializeDriver();

            //Opening the Test URL.
            SeleniumWrapper.OpenBrowser();
        }

        [TearDown]
        protected void CleanUp()
        {
            SingletonDriver.Quit();
        }

    }
}
