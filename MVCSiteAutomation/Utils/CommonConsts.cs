﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCAutomation
{
    abstract class CommonConsts
    {
        protected static String FIREFOX_DRIVER = "firefox";
        protected static String CHROME_DRIVER = "chrome";
        protected static String IE_DRIVER = "ie";
        protected static int TIMEOUT = 5;
    }
}
