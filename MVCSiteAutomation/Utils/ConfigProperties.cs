﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCAutomation.Utils
{
    class ConfigProperties
    {
        public static String GetConfigSection(String key)
        {
            var automationSettings = System.Configuration.ConfigurationManager.GetSection("appSettings")
                as NameValueCollection;

            if(automationSettings.Count == 0)
            {
                return "Automation Settings are not defined";
            } 
            else
            {
                return automationSettings[key];   
            }
        }
    }
}
