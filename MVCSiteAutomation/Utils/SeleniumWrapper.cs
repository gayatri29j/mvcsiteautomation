﻿using System;
using OpenQA.Selenium;
using MVCSiteAutomation;

namespace MVCAutomation.Utils
{
    class SeleniumWrapper : CommonConsts
    {
        private static String url = null;

        public static void OpenBrowser()
        {
            url = ConfigProperties.GetConfigSection("TestURL");
            Console.WriteLine("Test URL is : " + url);

            if (PropertyCollection.driver != null)
            {
                PropertyCollection.driver.Navigate().GoToUrl(url);
            }
            else
            {
                Console.WriteLine("Driver is not initialized");
            }
        }
    }
}
