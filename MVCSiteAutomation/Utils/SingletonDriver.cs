﻿using System;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using MVCSiteAutomation;

namespace MVCAutomation.Utils
{
    class SingletonDriver : CommonConsts
    {
        private static String browserName = null;

        public static void InitializeDriver()
        {
            Console.WriteLine("Intialize driver called");

            browserName = ConfigProperties.GetConfigSection("Browser");

            Console.WriteLine("Browser Name is : " + browserName);

            if (PropertyCollection.driver == null)
            {
               Console.WriteLine("Initializing the browser " + browserName);

                if (browserName.Equals(CHROME_DRIVER))
                    PropertyCollection.driver = new ChromeDriver();
                else if (browserName.Equals(FIREFOX_DRIVER))
                    PropertyCollection.driver = new FirefoxDriver();
                else if (browserName.Equals(IE_DRIVER))
                    PropertyCollection.driver = new InternetExplorerDriver();
            }
            else
            {
                Console.WriteLine(browserName + " : Browser type is not supported");
            }
        }

        public static void Close()
        {
            Console.WriteLine("Closing the browser " + browserName);
            PropertyCollection.driver.Close();
            PropertyCollection.driver = null;
        }

        public static void Quit()
        {
            Console.WriteLine("Quitting all windows " + browserName);
            PropertyCollection.driver.Quit();
            PropertyCollection.driver = null;
        }

    }
}
